CC      = g++
CCFLAGS = -Wall -Wextra -std=c++11

UNAME := $(shell uname)

ifeq ($(UNAME), Linux)
  LDFLAGS =
endif
ifeq ($(UNAME), Darwin)
  LDFLAGS = -framework CoreFoundation -framework CoreServices
endif

all: iwatch

iwatch: iwatch.cc
	$(CC) $(CCFLAGS) $(LDFLAGS) iwatch.cc -o iwatch

clean:
	rm -f iwatch

.PHONY: clean
