iwatch -- like watch(1), but with inotify
=========================================

When I write some documentation using Sphinx or some such, I usually
have a process like this running on one terminal which keeps building
HTML output periodically:

``` shell
$ watch make html
```

But watch runs every second, and I don't really need that.  I just
want Sphinx to build its HTML output only when some input files are
modified, kind of like what "omake -p" does, and kind of like jekyll,
hakyll, and many other static site builders do.

So I thought I would make a thing that runs arbitrary commands when
some watched files are modified.

``` shell
$ iwatch -w <path> -w <other path> -w <another path> -- cmd --args
```  

Note that iwatch is just a silly convenience tool that I wrote for
fun.  You can already do what iwatch does with inotify-tools:

``` shell
$ while inotifywait -r . ; do make html; done
```

That might be actually less buggy than iwatch, but oh well. :-)
