//
//  iwatch -- kind of like watch(1), but with inotify.
//
//  This program sets up some inotify watchers on a set of given
//  paths, and runs an arbitray command when any of those the watched
//  paths are modified.
//
//  Copyright (C) 2019 Sajith Sasidharan <sajith@hcoop.net>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------

#include <iostream>
#include <set>
#include <string>
#include <sstream>
#include <cstring>

#include <poll.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdlib.h>

#ifdef __linux__
  #include <sys/inotify.h>
  #include <error.h>
  #include <argp.h>
#elif __APPLE__
  #include <CoreServices/CoreServices.h>
  // TOOD: also check for iOS targets
#else
  #error "Error: unsupported OS"
#endif


// ----------------------------------------------------------------------

static bool verbose = false;

// ----------------------------------------------------------------------

// Used by ‘main’ to communicate with ‘parse_opt’.
struct arguments
{
    std::set<std::string>   watched_paths;
    char                  **command;
    bool                    verbose;
};

// ----------------------------------------------------------------------

static bool run_command(char *const *cmds)
{
    if (!cmds || !cmds[0])
    {
        throw std::runtime_error("Command incomplete");
    }

    const char *cmd = cmds[0];

    pid_t pid = fork();

    if (pid < 0)
    {
        throw std::runtime_error("fork() failed");
    }

    if (pid == 0)
    {
        // Execute sub-process in the child process.
        if (execvp(cmd, cmds) < 0)
        {
            std::stringstream err;

            err  << "[child process " << getpid() << "] "
                 << "Error running \"" << cmd
                 << "\": execv() error: " << std::strerror(errno);

            throw std::runtime_error(err.str());

            // In case of error, exit the child process with a
            // non-zero return value.
            exit(1);
        }
    }
    else
    {
        int   status  = 0;
        pid_t child = waitpid(-1, &status, 0);

        if (WIFEXITED(status))
        {
            if (verbose)
            {
                std::cout << cmd << " (pid:" << child << ") "
                          << "exited with " <<  WEXITSTATUS(status) << "\n";
            }
        }
        else if (WIFSIGNALED(status))
        {
            if (verbose)
            {
                std::cout << cmd << " (pid:" << child << ") was "
                          << "terminated by signal " <<  WTERMSIG(status) << "\n";
            }
        }
        else if (WIFSTOPPED(status))
        {
            if (verbose)
            {
                std::cout << cmd << " (pid:" << pid << ") was stopped "
                          << "by signal " << WSTOPSIG(status) << ".";
            }
        }
        else if (WIFCONTINUED(status))
        {
            if (verbose)
            {
                std::cout << cmd << " (pid:" << pid << ") "
                          << "has received SIGCONT.";
            }
        }
        else
        {
            std::stringstream err;

            err << cmd << " (pid:" << pid << ") "
                << "is in unknown state; giving up.\n";

            throw std::runtime_error(err.str());
        }
    }

    return false;
}

// ----------------------------------------------------------------------


static bool is_directory(std::string const &path)
{
    struct stat st;

    if (lstat(path.c_str(), &st) != 0)
    {
        return false;
    }

    return S_ISDIR(st.st_mode);
}


static void expand_directory(std::string const     &path,
                             std::set<std::string> &watched_paths)
{
    if (not is_directory(path))
    {
        auto err = path + " is not a directory";
        throw std::runtime_error(err);
    }

    if (verbose)
    {
        std::cout << "Expanding " << path << "\n";
    }

    DIR *dirp = opendir(path.c_str());

    if (dirp == nullptr)
    {
        auto err = path + ": " + std::strerror(errno);
        throw std::runtime_error(err);
    }

    dirent *resultp = nullptr;

    while ((resultp = readdir(dirp)) != nullptr)
    {
        if (std::string(resultp->d_name) == "." or
            std::string(resultp->d_name) == "..")
        {
            continue;
        }

        std::string newpath = path + "/" + resultp->d_name;

        if (is_directory(newpath))
        {
            watched_paths.emplace(newpath);
            expand_directory(newpath, watched_paths);
        }
    }

    closedir(dirp);

    return;
}

// ----------------------------------------------------------------------

#ifdef __linux__

static void handle_inotify_event(int fd, char *const *cmds)
{
    while (true)
    {
        if (verbose)
        {
            std::cout << "In " << __func__ << " loop\n";
        }

        // Read some events.
        char buf[BUFSIZ];
        auto len = read(fd, buf, sizeof buf);

        if (len == -1 && errno != EAGAIN)
        {
            perror("read");
            exit(EXIT_FAILURE);
        }

        // If the nonblocking read() found no events to read, then it
        // returns -1 with errno set to EAGAIN. In that case, we exit
        // the loop.
        if (len <= 0)
            break;

        if (verbose)
        {
            std::cout << "Event? Running cmd\n";
        }

        run_command(cmds);
    }
}


static void setup_inotify(const arguments &arguments)
{
    // Create a file descriptor for inotify API
    auto fd = inotify_init1(IN_NONBLOCK);

    if (fd == -1)
    {
        perror("inotify_init1");
        exit(EXIT_FAILURE);
    }

    // Allocate memory for watch descriptors
    int *wd = static_cast<int *>(calloc(arguments.watched_paths.size(), sizeof(int)));

    if (wd == NULL)
    {
        perror("calloc");
        exit(EXIT_FAILURE);
    }

    if (verbose)
    {
        std::cout << "Watch files size: "
                  << arguments.watched_paths.size() << "\n";
    }

    // Mark directories for events
    size_t i = 0;

    for (auto const path : arguments.watched_paths)
    {
        // auto flags = IN_ALL_EVENTS;

        auto flags = IN_ATTRIB
            | IN_CLOSE_WRITE
            | IN_CREATE
            | IN_DELETE
            | IN_DELETE_SELF
            | IN_MODIFY
            | IN_MOVE_SELF
            | IN_MOVED_FROM
            | IN_MOVED_TO;

        if (verbose)
        {
            std::cout << "Adding " << path << " to watch list\n";
        }

        wd[i] = inotify_add_watch(fd, path.c_str(), flags);

        if (wd[i] == -1)
        {
            std::cerr << "Cannot watch " << path << ": "
                      << std::strerror(errno) << "\n";

            exit(EXIT_FAILURE);
        }

        i++;
    }

    // Start polling
    int nfds = 2;
    struct pollfd fds[nfds];

    // Poll stdin.
    fds[0].fd     = STDIN_FILENO;
    fds[0].events = POLLIN;

    // Poll inotify fd.
    fds[1].fd     = fd;
    fds[1].events = POLLIN;

    if (verbose)
    {
        std::cout << "poll(): listening for events.\n";
    }

    while (true)
    {
        int poll_num = poll(fds, nfds, -1);

        if (poll_num == -1)
        {
            if (errno == EINTR)
                continue;
            perror("poll");
            exit(EXIT_FAILURE);
        }

        if (poll_num > 0)
        {
            if (fds[0].revents & POLLIN)
            {
                // There's input in stdin
                char buf;
                while (read(STDIN_FILENO, &buf, 1) > 0 && buf != '\n')
                    continue;
                break;
            }

            if (fds[1].revents & POLLIN)
            {
                handle_inotify_event(fd, arguments.command);
            }
        }
    }

    if (verbose)
    {
        std::cout << "Listening for events stopped.\n";
    }

    free(wd);
    close(fd);
}

#endif // __linux__

// ----------------------------------------------------------------------

#ifdef __APPLE__

static void setup_fsevents(const arguments &arguments)
{
    FSEventStreamRef stream;

    FSEventStreamScheduleWithRunLoop(stream,
                                     CFRunLoopGetCurrent(),
                                     kCFRunLoopDefaultMode);
}

#endif

// ----------------------------------------------------------------------

static void print_usage(std::string const &prog)
{
    std::cout << "Usage: " << prog << " [-h] [-v] -w [PATH] -- [CMDS]\n\n"
              << "Run a command when watched paths are modified.\n\n"
              << "  -w [PATH]  will add PATH to watch list.\n"
              << "  -v         will make the program run verbosely.\n"
              << "  -h         will print this help message and exit.\n\n";
}

// ----------------------------------------------------------------------

int main (int argc, char **argv)
{
    struct arguments arguments;

    // Default argument values.
    arguments.verbose = false;

    int c;
    opterr = 0;

    while ((c = getopt(argc, argv, "vw:h")) != -1)
    {
        switch (c)
        {
        case 'v':
            arguments.verbose = true;
            break;
        case 'w':
            arguments.watched_paths.emplace(optarg);
            break;
        case 'h':
        case '?':
            print_usage(argv[0]);
            exit(1);
        default:
            abort ();
        }
    }

    if (arguments.verbose)
    {
        for (int index = optind; index < argc; index++)
        {
            std::cout << "Non-option argument " << argv[index] << "\n";
        }
    }

    if (optind < argc)
    {
        arguments.command = &argv[optind];
    }
    else
    {
        print_usage(argv[0]);
        exit(1);
    }

    // Set up the global flag
    verbose = arguments.verbose;

    // If no arguments were given, watch current directory.
    if (arguments.watched_paths.empty())
    {
        if (verbose)
        {
            std::cout << "No files/directories to watch.  "
                      << "Adding current directory.\n";
        }
        arguments.watched_paths.emplace(".");
    }

    for (auto const &path : arguments.watched_paths)
    {
        if (is_directory(path))
        {
            expand_directory(path, arguments.watched_paths);
        }
    }

    if (verbose)
    {
        std::cout << "FILES = \n";
        for (auto const &w : arguments.watched_paths)
        {
            std::cout << " - " << w << "\n";
        }
    }

    if (verbose)
    {
        std::cout << "CMD = ";

        for (int j = 0; arguments.command[j]; j++)
            std::cout << (j == 0 ? "" : ", ") << arguments.command[j];

        std::cout << "\n";
    }


    // Run once for the first time.
    run_command(arguments.command);


#ifdef __linux__
    setup_inotify(arguments);
#elif __APPLE__
    setup_fsevents(arguments);
#endif

    exit (0);
}

// ----------------------------------------------------------------------

// Local Variables:
// mode: c++
// c-basic-offset: 4
// compile-command: "g++ -Wall -Wextra -std=c++11 iwatch.cc -o iwatch"
// End:
